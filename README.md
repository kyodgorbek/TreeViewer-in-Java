# TreeViewer-in-Java



import java.awt.*;
import java.awt.event*;
import java.io.*;
import javax.swing.*;
import javax.swing.tree.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.w3c.dom.CharacterData;

public class TreeViewer {

 public static void main(String[] args){
    
    
    EventQueue.invokeLater(new Runnable()
    {
      public void run()
      {
        JFrame frame = new DOMTreeFrame();
        frame.setTitle("TreeViewer");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
      }
    });
  }
}

class DOMTreeFrame extends JFrame
{
  private static final int DEFAULT_WITDH = 400;
  private static final int DEFAULT_WIDTH = 400;
  
  private DocumentBuilder builder;
  
  public DOMTreeFrame()
  {
    setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
    
    JMenu fileMenu = new JMenu("File");
    JMenuItem openItem = new JMenuItem("open");
    openItem.addActionListener(new ActionListener()      
     {
       
       public void actionPerformed(ActionEvent event)
       {
          openFile();
       }
     });
    fileMenu.add(openItem);
    
    JMenuItem exitItem = new JMenuItem("EXIT");
    exitItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent event)
      {
         System.exit(0);
      }
    });
   fileMenu.add(exitItem);
   
   JMenuBar menuBar = new JMenuBar();
   menuBar.add(fileMenu);
   setJMenuBar(menuBar);
 }
 
 public void openFile()
 {
   JFileChooser chooser = new JFileChooser();
   chooser.setCurrentDirectory(new File("dom"));
   
   chooser.setFileFilter(new javax.swing.filechooser.FileFilter()
   {
     public boolean accept(File f)
     {
        return f.isDirectory() || f.getName().toLowerCase().endsWidth(".xml");  
     }        
     
     public String getDescription()
     {
         return "XML files";
     }
   });
   
   int r = chooser.showOpenDialog(this);
   if (r!= JFileChooser.APPROVE_OPTION) return;
   final File file = chooser.getSelectedFile();
   
   new SwingWorker<Document, Void>()
   {
      protected Document doInBackground() throws Exception
      {
        if (builder == null)
        {
           DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
           builder = factory.newDocumentBuilder();
           
        }  
        return builder.parse(file);
      }
    
    protected void done()
    {
      try
      {
        Document doc = get();
        JTree tree = new JTree(new DOMTreeModel(doc));
        tree.setCellRenderer(new DOMTreeCellRenderer());
        
        setContentPane(new JScroolPane(tree));
        validate();
      }
      
      catch (Exception e)
      {
        JOptionPane.showMessageDialog(DOMTreeFrame.this, e);
      }
    }
   }.execute();         
 }
}

class DOMTreeModel implements TreeModel
{
   private Document doc;
   
   public DOMTreeModel(Document doc)
   {
      this.doc;
   }
   
   public Object getRoot()
   {
      return doc.getDocumentElement();
   }
   
   public int getChildCount(Object parent)
    {
       Node node = (Node) parent;
       NodeList list = node.getChildNodes();
       return list.getLength();
    }
    
    public Object getChild(Object parent, int index)
    {
      Node node = (Node) parent;
      NodeList list = node.getChildNodes();
      return list.item(index);
    }
    
    public int getIndexOfChild(Object parent, Object child)
    {
       Node node = (Node) parent;
       NodeList list = node.getChildNodes();
       for (int i = 0; i < list.getLength(); i++)
       if(getChild(node, i) == child) return i;
       return -1;
    }
    
    public boolean isLeaf(Object node)
    {
       return getChildCount(node) == 0;
       
    }
    
    public void valueforPathChanged(TreePath path, Object new Value)
    {
    }
    
    public void addTreeModelListener(TreeModelListener 1)
    {
    }
    
    public void removeTreeModelListener(TreeModelListener 1)
    {
    }
 }
 
 class DOMTreeCellRenderer extends DefaultTreeCellRenderer
 {
   public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus)
   {
     Node node = (Node) value;
     if (node instanceOf Element) return elementPanel((Element) node);
     
     super.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);
     
     if (node instanceof CharacterData) setText(characterString((CharacterData) node));
     else setText(node.getClass() + ":" + node.toString();
     return this;
   }
   
   public static JPanel elementPanel(Element e)
   {
      JPanel panel = new JPanel();
      panel.add(new JLabel("Element: " + e.getTagName()));
      final NameNodeMap map = e.getAttributes();
      panel.add(new JTable(new AbstractTableModel()
      {
         public int getRowCount()
         {
            return map.getLength();
         }
         
         public int getColumnCount()
         {
            return 2;
         }
         
        
        public Object getValueAt(int r, int c)
        {
           return c == 0 ? map.item(r).getNodeName() : map.item(r),getNodeValue();
        }      
    }));
  return panel;
}

public static String characterString(CharacterData node)
{
   StringBuilder builder = new StringBuilder(node.getData());
   for (int i = 0; i < bulder.length(); i++)
   {
     if (builder.charAt(i) == '\r')
     {
       builder.replace(i, i + 1, "\\r");
       i++;
     }
     
     else if (builder.charAt(i) == '\n')
     {
        builder.replace(i, i + 1, "'\\n");
        i++;
     }
     
     else if (builder.charAt(i) == '\t')
     {
        builder.replace(i, i + 1, "\\t");
     }  
   }
   
   if (node instanceof CDATASection) builder.insert(0, "CDATASection: ");
   else if (node instancof Text) builder.insert(0, "Text:");
   else if (node instanceof Comment) builder.insert(0, "Comment");
   
   return builder.toString();
  }
}            
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
        
